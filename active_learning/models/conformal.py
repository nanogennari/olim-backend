from abc import abstractmethod

from sklearn.model_selection import train_test_split
import numpy as np

from . import ClassificationModel

BIG_N: float = (
    10e30  # numpy doesn't like infinity in quantiles, so we'll just use an outrageously large number instead.
)


class UncertantyPredictor(ClassificationModel):
    @abstractmethod
    def predict_uncert(self, unlabelled_data: list[str]) -> list[dict[int, float]]:
        pass


class ConformalPredictor(UncertantyPredictor):
    def __init__(
        self,
        model: ClassificationModel,
        calibration_split_size: float = 0.20,
        alpha: float = 0.1,
        n_classes: int | None = None,
    ):
        self.model: ClassificationModel = model
        self.calibration_split_size: float = calibration_split_size
        self.threshold: list[float] | None = None
        self.alpha: float = alpha
        self.n_classes: int | None = n_classes

    def _score(self, y: int):
        return 1 - y

    def train(
        self,
        labelled_data: list[tuple[str, int]],
        validation_data: list[tuple[str, int]] | None = None,
        skip_model_train: bool = False,
        alpha: float | None = None,
        **kwargs,
    ) -> None:
        alpha = alpha or self.alpha
        if validation_data is None or len(validation_data) == 0:
            train_labelled_data, cal_labelled_data = train_test_split(
                labelled_data, test_size=self.calibration_split_size, random_state=0
            )
        else:
            train_labelled_data = labelled_data
            cal_labelled_data = validation_data

        if self.n_classes is None:
            y = [i for _, i in labelled_data]
            labels = np.sort(np.unique(y))
            assert all(
                [i == j for i, j in zip(labels, np.arange(np.max(y) + 1))]
            ), "Failed to detect classes, try setting n_classes"
        else:
            labels = np.arange(self.n_classes)

        # assert np.all(np.isin([i for _, i in labelled_data], labels))

        # Train model
        if not skip_model_train:
            self.model.train(train_labelled_data, **kwargs)

        # Predict probs for calibration data
        cal_unlabeled_data = [s for s, _ in cal_labelled_data]
        cal_labels = np.array([i for _, i in cal_labelled_data])
        cal_probs_dicts = self.model.predict_proba(cal_unlabeled_data)
        cal_probs = np.zeros(len(cal_probs_dicts))

        # Calculate threshold for each label
        self.threshold = np.zeros(labels.shape)
        for i in labels:
            for j in range(len(cal_probs)):
                cal_probs[j] = cal_probs_dicts[j][i]
            mask_label = cal_labels == i
            scores = self._score(cal_probs[mask_label])
            self.threshold[i] = np.quantile(
                np.concatenate((scores, [BIG_N])), 1 - self.alpha
            )

    def get_embeddings(self, data: list[str]) -> list[list[float]]:
        return self.model.get_embeddings(data)

    def predict(self, unlabelled_data: list[str]) -> list[list[int]]:
        probas = self.model.predict_proba2(unlabelled_data)
        preds = [
            [i for i, t in enumerate(self.threshold) if self._score(prob[i]) <= t]
            for prob in probas
        ]
        for i in range(len(preds)):
            if len(preds[i]) == 0:
                preds[i] = [np.argmax(probas[i, :])]
        return preds

    def predict_proba(self, unlabelled_data: list[str]) -> list[dict[int, float]]:
        return self.model.predict_proba(unlabelled_data)

    def predict_uncert(self, unlabelled_data: list[str]) -> list[dict[int, float]]:
        probas = self.model.predict_proba(unlabelled_data)
        score = np.array(
            [
                np.mean(
                    [
                        self._score(prob[i])
                        for i, t in enumerate(self.threshold)
                        if self._score(prob[i]) <= t
                    ]
                )
                for prob in probas
            ]
        )
        return np.nan_to_num(score)
