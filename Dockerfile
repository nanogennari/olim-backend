FROM python:3.11-slim

EXPOSE 42010

# Keeps Python from generating .pyc files in the container
ENV PYTHONDONTWRITEBYTECODE=1

# Turns off buffering for easier container logging
ENV PYTHONUNBUFFERED=1

# Install requirements
RUN python -m pip install poetry

WORKDIR /app
COPY . /app

RUN cd /app

RUN poetry config installer.max-workers 10

RUN poetry install --no-interaction --no-ansi -vvv

RUN apt-get update && \
    apt-get install -y --no-install-recommends \
        ca-certificates \
        cmake \
        build-essential \
        gcc \
        g++ \
        git && \
    rm -rf /var/lib/apt/lists/* && \
    apt-get install libgomp1 -y

USER root

#RUN export SECRET_KEY=`python -c 'import secrets; print(secrets.token_hex())'`

# During debugging, this entry point will be overridden. For more information, please refer to https://aka.ms/vscode-docker-python-debug
CMD ["poetry", "run", "gunicorn", "--reload", "--bind", "0.0.0.0:42010", "api:app"]
