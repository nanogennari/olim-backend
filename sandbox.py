import json
from pathlib import Path
import argparse
from copy import deepcopy

import plotly.express as px
import plotly.graph_objects as go
import pandas as pd
import numpy as np
from icecream import ic
from tqdm import tqdm

from active_learning.public_api import ActiveLearningBackend

# parser = argparse.ArgumentParser()

# parser.add_argument("bkp_file")

# args = parser.parse_args()

# ic(args)

# df = pd.read_csv("/home/nano/Documents/OLIM/olim-backend/data/Id_Text.csv")

# ic(df.head(10))

# path = Path("teste")

# unlabelled_data = df["Text"].to_dict()
# # {int(row["Id"]): row["Text"] for _, row in tqdm(df.iterrows())}

N = 200

to_plot = [
    # ("nEpzTb6snwuqiaWW3VfU1pDdtN5KaaNs", "200", px.colors.qualitative.Plotly[2]),
    ("Uvf4bfWSTWpQTtI4Ihw6dGQ7hp5RJtQH", "high uncer.", px.colors.qualitative.Plotly[0],
    np.array([0.        , 0.        , 0.        , 0.        , 0.        ,
                0.        , 0.        , 0.        , 0.        , 0.        ,
                0.        , 0.        , 0.        , 0.        , 0.        ,
                0.        , 0.        , 0.        , 0.        , 0.        ,
                0.        , 0.        , 0.        , 0.        , 0.        ,
                0.        , 0.        , 0.        , 0.        , 0.        ,
                0.        , 0.        , 0.        , 0.        , 0.        ,
                0.        , 0.        , 0.5       , 0.5       , 0.5       ,
                0.5       , 0.5       , 0.5       , 0.5       , 0.5       ,
                0.5       , 0.5       , 0.5       , 0.5       , 0.5       ,
                0.5       , 0.5       , 0.5       , 0.5       , 0.5       ,
                0.66666667, 0.66666667, 0.66666667, 0.75      , 0.75      ,
                0.25      , 0.75      , 0.75      , 0.75      , 0.5       ,
                0.75      , 0.5       , 0.5       , 0.5       , 0.5       ,
                0.75      , 0.8       , 0.8       , 0.8       , 0.6       ,
                0.6       , 0.6       , 0.6       , 0.6       , 0.6       ,
                0.6       , 0.6       , 0.6       , 0.6       , 0.6       ,
                0.6       , 0.6       , 0.8       , 0.6       , 0.6       ,
                0.6       , 0.6       , 0.66666667, 0.66666667, 0.66666667,
                0.66666667, 0.83333333, 0.83333333, 0.66666667, 0.83333333]),
    np.array([0.        , 0.        , 0.        , 0.        , 0.        ,
                 0.        , 0.        , 0.        , 0.        , 0.        ,
                 1.        , 1.        , 1.        , 1.        , 1.        ,
                 1.        , 1.        , 1.        , 1.        , 1.        ,
                 1.        , 1.        , 1.        , 1.        , 1.        ,
                 1.        , 1.        , 1.        , 1.        , 1.        ,
                 1.        , 1.        , 1.        , 1.        , 1.        ,
                 1.        , 1.        , 1.        , 1.        , 1.        ,
                 1.        , 1.        , 1.        , 1.        , 1.        ,
                 1.        , 1.        , 1.        , 1.        , 1.        ,
                 1.        , 1.        , 1.        , 1.        , 1.        ,
                 1.        , 1.        , 1.        , 1.        , 1.        ,
                 0.5       , 1.        , 1.        , 1.        , 0.75      ,
                 1.        , 0.75      , 0.75      , 0.75      , 0.75      ,
                 1.        , 1.        , 1.        , 1.        , 0.8       ,
                 0.8       , 0.8       , 0.8       , 0.8       , 0.8       ,
                 0.8       , 0.8       , 0.8       , 0.8       , 0.8       ,
                 0.8       , 0.8       , 1.        , 0.8       , 0.8       ,
                 0.8       , 0.8       , 0.83333333, 0.83333333, 0.83333333,
                 0.83333333, 1.        , 1.        , 0.83333333, 1.        ])),
    ("1LQ6lX2HLBRjGzGog7TnYigsUXzcttJM", "mixed uncer.", px.colors.qualitative.Plotly[1],
    np.array([0.        , 0.        , 0.        , 0.        , 0.        ,
                0.        , 0.        , 0.        , 0.        , 0.        ,
                0.25      , 0.25      , 0.25      , 0.25      , 0.25      ,
                0.25      , 0.25      , 0.5       , 0.        , 0.        ,
                0.        , 0.        , 0.        , 0.        , 0.        ,
                0.        , 0.        , 0.        , 0.        , 0.        ,
                0.        , 0.        , 0.        , 0.5       , 0.5       ,
                0.5       , 0.5       , 0.5       , 0.5       , 0.5       ,
                0.5       , 0.5       , 0.5       , 0.5       , 0.5       ,
                0.5       , 0.5       , 0.5       , 0.5       , 0.5       ,
                0.5       , 0.5       , 0.5       , 0.5       , 0.5       ,
                0.5       , 0.5       , 0.5       , 0.5       , 0.5       ,
                0.5       , 0.5       , 0.5       , 0.5       , 0.5       ,
                0.5       , 0.5       , 0.5       , 0.66666667, 0.66666667,
                0.66666667, 0.75      , 0.75      , 0.75      , 0.75      ,
                0.75      , 0.75      , 0.75      , 0.75      , 0.75      ,
                0.75      , 0.75      , 0.75      , 0.8       , 0.83333333,
                0.83333333, 0.83333333, 0.83333333, 0.83333333, 0.85714286,
                0.85714286, 0.85714286, 0.875     , 0.875     , 0.88888889,
                0.88888889, 0.88888889, 0.88888889, 0.88888889, 0.88888889]),
    np.array([0.  , 0.  , 0.  , 0.  , 0.  , 0.  , 0.  , 0.  , 0.  , 0.  , 0.75,
                 0.75, 0.75, 0.75, 0.75, 0.75, 0.75, 1.  , 0.5 , 0.5 , 0.5 , 0.5 ,
                 0.5 , 0.5 , 0.5 , 0.5 , 0.5 , 0.5 , 0.5 , 0.5 , 0.5 , 0.5 , 0.5 ,
                 1.  , 1.  , 1.  , 1.  , 1.  , 1.  , 1.  , 1.  , 1.  , 1.  , 1.  ,
                 1.  , 1.  , 1.  , 1.  , 1.  , 1.  , 1.  , 1.  , 1.  , 1.  , 1.  ,
                 1.  , 1.  , 1.  , 1.  , 1.  , 1.  , 1.  , 1.  , 1.  , 1.  , 1.  ,
                 1.  , 1.  , 1.  , 1.  , 1.  , 1.  , 1.  , 1.  , 1.  , 1.  , 1.  ,
                 1.  , 1.  , 1.  , 1.  , 1.  , 1.  , 1.  , 1.  , 1.  , 1.  , 1.  ,
                 1.  , 1.  , 1.  , 1.  , 1.  , 1.  , 1.  , 1.  , 1.  , 1.  , 1.  ,
                 1.  ])),
]

def get_auc_curve(lid):
    labels = ["no", "yes"]

    rng = np.random.default_rng(42)

    with open("/home/nano/Documents/OLIM/olim-backend/work/ULGD2x3aIUAsFIDtMZmEpF4y8R6mzEbR/active-learning/"+lid+"/learner/fields.json", "r") as f:
        data = json.load(f)

    messages = data['messages']
    dataset = data['_dataset']
    dataset.update(data['_val_dataset'])

    ids = [m.split(' ')[-1] for m in messages if m.startswith("removing from unlabeled:")]

    with open("/home/nano/Documents/OLIM/olim-backend/work/ULGD2x3aIUAsFIDtMZmEpF4y8R6mzEbR/data.json", "r") as f:
        unlabelled_data = json.load(f)

    learner = ActiveLearningBackend(
        deepcopy(unlabelled_data),
        labels,
        n_kickstart=5,
        subsample_size=[500, 6],
        rng=rng,
    )

    low = np.zeros(N)
    high = np.zeros(N)

    for i, entry_id in enumerate(tqdm(ids[:N])):
        learner._training = True
        learner._given_nexts = ids
        learner.submit_labelling(entry_id, labels[dataset[entry_id][1]])
        learner._retrain = False
        if i >=10:
            learner._train()
            l, h = learner.peek_auc_roc_ovr(alpha=0.1)
            low[i] = l
            high[i] = h
    return low, high

fig = go.Figure()
for data in to_plot:
    if len(data) > 3:
        l, n, c, low, high = data
    else:
        l, n, c = data
        low, high = get_auc_curve(l)

    x = np.arange(1, 101)
    y = (low + high)/2

    fig.add_trace(go.Scatter(x=x[10:], y=low[10:],
        fill=None,
        mode='lines',
        line = dict(color=c, dash='dash'),
        showlegend=False,
        ))
    fig.add_trace(go.Scatter(
        x=x[10:],
        y=high[10:],
        fill='tonexty', # fill area between trace0 and trace1
        mode='lines',
        line = dict(color=c, dash='dash'),
        showlegend=False,
    ))
    fig.add_trace(go.Scatter(
        x=x[10:],
        y=y[10:],
        fill=None,
        mode='lines',
        line_color=c,
        name=n,
    ))
    ic(low)
    ic(high)

fig.update_layout( # customize font and legend orientation & position
    width=500,
    height=350,
    legend=dict(
        x=0.7,
        y=0.05,
        traceorder="normal",
    ),
    xaxis=dict(
        title=dict(
            text="Number of manual labels"
        )
    ),
    yaxis=dict(
        title=dict(
            text="AUC-ROC"
        )
    ),
    template="simple_white"
)
fig.update_xaxes(range=[0, 100])
fig.update_yaxes(range=[0, 1])
fig.write_image("auc_roc.pdf")
fig.show()



# labelled_data = {}

# try:
#     with open(args.bkp_file, "r") as f:
#         labelled_data = json.load(f)
#         labelled_data = {k: v for k, v in labelled_data.items()}
# except:
#     print("Couldn't load previous labels")



# for k, v in unlabelled_data.items():
#     if k is None or v is None:
#         print(k, v)

# if len(labelled_data) > 20:
#     initial_labeled = {}
#     for label in labels:
#         ids = [k for k, v in labelled_data.items() if v == label]
#         if len(ids) == 0:
#             initial_labeled = None
#             break
#         ids = rng.choice(ids, size=10, replace=False)
#         for entry_id in ids:
#             initial_labeled[entry_id] = label
# else:
#     initial_labeled = None

# ic(labelled_data)

# learner = ActiveLearningBackend(
#     deepcopy(unlabelled_data),
#     labels,
#     n_kickstart=10,
#     initial_labelled_dataset=initial_labeled,
#     subsample_size=1000,
#     subsubsample_size=10,
#     rng=rng,
# )


# while True:
#     entry_id = learner.request_next_entry()

#     print()
#     print(f"[{entry_id}]: {unlabelled_data[entry_id]}")
#     print()
#     if entry_id in labelled_data:
#         awnser = labelled_data[entry_id]
#         print(f"(y)es/(n)o? {awnser}")
#     else:
#         awnser = input("(y)es/(n)o? ").lower()

#     if awnser[0] == "n":
#         awnser = labels[0]
#     elif awnser[0] == "y":
#         awnser = labels[1]
#     else:
#         continue

#     labelled_data[entry_id] = awnser
#     learner.submit_labelling(entry_id, awnser)

#     with learner.msg_lock:
#         for m in learner.messages:
#             print(m)
#         learner.messages = []
#     print(np.unique(list(labelled_data.values()), return_counts=True))

#     with open(args.bkp_file, "w") as f:
#         json.dump(labelled_data, f)

#     from pathlib import Path

#     # learner.save(Path("test_save"))
#     # learner = ActiveLearningBackend.load(Path("test_save"), unlabelled_data, rng=rng)
