# OLIM Learner

Backend container for [OLIM](https://gitlab.com/nanogennari/olim).

To use it first load the data and generate a app key:

`
    docker compose exec olim-learner poetry run flask --app api app-keys new data/sample_data.csv text_id text
`

**Currently the backend is only compatible with text entries!**

**You'll need to have olim frontend running to use the learner.** Instanciate the lerner first, get the app-key and then instanciate OLIM with it.

## Model settings

Use the following environment variables to setup models:

- **CLASSIFICATION_MODEL**: "TfidfXGBoostClassifier" or "DebertaV3Wrapper" (this model needs a tensorflow based container, see coments on the `docker-compose.yaml` file)
- **SKIP_AL**: Used for testing, skips active learning algorithms and select random entries for labelling.
- **UNCERTAIN_PERC**: Percentage of uncertain entries for labeleing, the remainder will be certain predictions.
