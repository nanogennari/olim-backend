import os

WORK_FOLDER = os.getenv("WORK_FOLDER", "/app/work")

RANDOM_SEED = os.getenv("RANDOM_SEED", None)

if RANDOM_SEED is not None:
    RANDOM_SEED = int(RANDOM_SEED)
