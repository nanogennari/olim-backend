from . import app
from . import aux

import click
import pandas as pd


@app.cli.group("app-keys", help="App keys related commands.")
def app_keys():
    pass


@click.command(
    "new",
    help="Generates a new app key."
    "\n\n\tCSV_FILE\tPath to the CSV file to load data."
    "\n\n\tID_COLUMN\tColumn name to use as id for the entries (must be unique with all other entries in OLIM)."
    "\n\n\tTEXT_COLUMN\tColumn name to use as the text.",
)
@click.argument("csv_file", type=click.Path(exists=True))
@click.argument("id_column")
@click.argument("text_column")
def new_key_cli(csv_file, id_column, text_column):
    print("Generating key...\n")
    key = aux.gen_key()
    wf = aux.get_work_folder()
    app_folder = wf.joinpath(key)
    print("Cretating folder...\n")
    app_folder.mkdir()
    print("Loading data...\n")
    df = pd.read_csv(csv_file)
    df = df.loc[:, [id_column, text_column]].dropna()
    df = df.set_index(id_column)
    df[text_column].to_json(app_folder.joinpath("data.json"), orient="index")
    print(f"New app key: {key}")


@click.command("revoke", help="Revokes a app key.")
@click.argument("key")
def revoke_key_cli(key):
    wf = aux.get_work_folder()
    revoked_dir = wf.joinpath("revoked")
    if not revoked_dir.is_dir():
        revoked_dir.mkdir()
    key_folder = wf.joinpath(key)
    if not key_folder.is_dir():
        print("Key not found")
        return None
    key_folder.rename(revoked_dir.joinpath(key))
    print(f"App key {key} revoked!")
    print("All data moved to revoked dir.")


@click.command("list", help="Lists all app keys.")
def list_keys_cli():
    print("Active keys:\n")
    wf = aux.get_work_folder()
    for key in [x.stem for x in wf.iterdir() if x.is_dir() and x.stem != "revoked"]:
        print(key)
    revoked_dir = wf.joinpath("revoked")
    if revoked_dir.is_dir():
        print("\nRevoked keys:\n")
        for key in [x.stem for x in revoked_dir.iterdir() if x.is_dir()]:
            print(key)
    print()


app_keys.add_command(new_key_cli)
app_keys.add_command(list_keys_cli)
app_keys.add_command(revoke_key_cli)
