from . import settings

import string
from pathlib import Path
from typing import Optional
from time import time
from icecream import ic

import numpy as np
from werkzeug.exceptions import Forbidden
from numpy.random import default_rng

def get_rng():
    if settings.RANDOM_SEED is None:
        seed = int(time())
    else:
        seed = settings.RANDOM_SEED
    ic(f"Created generator using seed: {seed}")
    return default_rng(seed=seed)

def get_work_folder() -> Path:
    work_folder = Path(settings.WORK_FOLDER)
    if not work_folder.is_dir():
        work_folder.mkdir(parents=True)
    return work_folder.absolute()


def check_app(app_key: str) -> Optional[Path]:
    wf = get_work_folder()
    if app_key in [x.stem for x in wf.iterdir() if x.is_dir() and x.stem != "revoked"]:
        return wf.joinpath(app_key)
    else:
        raise Forbidden(f"App key {app_key} is not autorized.")


def gen_key(size=32):
    return "".join(
        np.random.choice(list(string.ascii_letters + string.digits), size=size)
    )
