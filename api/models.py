from datetime import datetime
from typing import Optional, Any

from pydantic import BaseModel


class BaseRequest(BaseModel):
    # timestamp: datetime
    app_key: str
    user_id: Optional[int] = None


class InitialRequestModel(BaseRequest):
    label: str
    values: list[str]


class LabelValueModel(BaseRequest):
    label_id: str
    entry_id: int | str
    value: int | str


class LabelRequestModel(BaseRequest):
    label_id: str


class SyncLabelModel(BaseRequest):
    values: list[str]
    label: dict[str, Any]
