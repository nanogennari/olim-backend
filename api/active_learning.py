import json
from threading import Thread
from pathlib import Path
from time import sleep

from flask_restful import Resource
from flask import request
from icecream import ic

from . import api
from . import models
from . import aux
import active_learning as al

learners_cache = {}


def get_data(app_key):
    app_dir = aux.get_work_folder().joinpath(app_key)
    with app_dir.joinpath("data.json").open("r") as f:
        data = json.load(f)
    return data


def get_label_dir(app_key, label_id) -> Path:
    app_dir = aux.check_app(app_key)
    if app_key:
        label_dir = app_dir.joinpath("active-learning", label_id)
        if not label_dir.is_dir():
            label_dir.mkdir(parents=True)
        return label_dir


def instanciate_al(app_key, label_id, values):
    data = get_data(app_key)
    learner_path = get_label_dir(app_key, label_id).joinpath("learner")
    print(f"Instanciating learner for {app_key}, {label_id}")
    learner = al.public_api.ActiveLearningBackend(
        data, values, save_path=learner_path, rng=aux.get_rng()
    )
    print(f"Storing learner for {app_key}, {label_id}")
    learner.save(learner_path)
    learners_cache[label_id] = learner


def get_learner(app_key, label_id):
    if label_id in learners_cache:
        return learners_cache[label_id]
    else:
        data = get_data(app_key)
        print(f"Learner for {app_key}, {label_id} not on cache, loading.")
        learner_path = get_label_dir(app_key, label_id).joinpath("learner")
        learner = al.public_api.ActiveLearningBackend.load(
            learner_path, data, rng=aux.get_rng()
        )
        learners_cache[label_id] = learner
        return learner


def save_learner(app_key, label_id):
    learner = learners_cache[label_id]
    learner_file = get_label_dir(app_key, label_id).joinpath("learner")
    # print(learner._dataset)
    learner.save(learner_file)


def create_learner(app_key, values):
    label_id = aux.gen_key()
    label_dir = get_label_dir(app_key, label_id)
    Thread(
        target=instanciate_al,
        kwargs={"app_key": app_key, "label_id": label_id, "values": values},
    ).start()
    return label_id, label_dir


class InitialRequest(Resource):
    def put(self):
        req = models.InitialRequestModel(**json.loads(request.json))
        label_id, label_dir = create_learner(req.app_key, req.values)

        with label_dir.joinpath("initial_request.json").open("w") as f:
            f.write(req.model_dump_json())

        return {"label_id": label_id}


class LabelRequest(Resource):
    def put(self):
        req = models.LabelRequestModel(**dict(request.form))
        learner = get_learner(req.app_key, req.label_id)
        return {
            "label_id": req.label_id,
            "entry_id": learner.request_next_entry(),
            "messages": learner.metrics_strs,
        }


class LabelValue(Resource):
    def put(self):
        req = models.LabelValueModel(**dict(request.form))
        learner = get_learner(req.app_key, req.label_id)
        learner.submit_labelling(req.entry_id, al.public_api.Labelling(req.value))
        save_learner(req.app_key, req.label_id)
        return {"label_id": req.label_id, "entry_id": req.entry_id}


class LabelDelete(Resource):
    def delete(self):
        req = models.LabelRequestModel(**json.loads(request.json))
        learners_cache.pop(req.label_id)
        label_dir = get_label_dir(req.app_key, req.label_id)
        label_dir.rmdir()
        return {"label_id": req.label_id}


class SyncLabel(Resource):
    def put(self):
        req = models.SyncLabelModel(**json.loads(request.json))
        values, label = req.values, req.label
        label_id = label.get("label_id")

        if not label_id:
            label_id, _ = create_learner(req.app_key, values)

        # FIXME Load learner
        sleep(2)
        learner = get_learner(req.app_key, label_id)
        learner.sync_labelling(label["entries"])
        learner_file = get_label_dir(req.app_key, label_id).joinpath("learner")
        learner.save(learner_file)

        return {"al_key": label_id}


api.add_resource(InitialRequest, "/al/new-label")
api.add_resource(LabelRequest, "/al/req-entry")
api.add_resource(LabelValue, "/al/add-value")
api.add_resource(LabelDelete, "/al/delete-label")
api.add_resource(SyncLabel, "/al/sync-label")
